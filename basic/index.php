<?php

require_once 'lib/php-cassandra/Cassandra/Cassandra.php';

$cassandra = new Cassandra();
var_dump($cassandra);

$s_server_host     = '127.0.0.1';    // Localhost
$i_server_port     = 9042;
$s_server_username = 'admin';  // We don't use username
$s_server_password = 'password';  // We don't use password
$s_server_keyspace = 'demo';  // We don't have created it yet

$cassandra->connect($s_server_host, $s_server_username, $s_server_password, $s_server_keyspace, $i_server_port);
var_dump($cassandra);

// Tests if the connection was successful:
if ($cassandra) {

    // Select:
    // Queries a table.
    $cql = "SELECT * FROM users;";

    // Launch the query.
    $results = $cassandra->query($cql);
    var_dump($results);

    // Update:
    // Prepares a statement.
    $stmt = $cassandra->prepare('UPDATE users SET first_name = ?, last_name = ? where id = ?');

    // Executes a prepared statement.
    $values = array('first_name' => 'Fred', 'last_name' => 'Smith', 'id' => '1');
    $result = $cassandra->execute($stmt, $values);
    var_dump($result);

    // Insert:
    // Prepares a statement.
    $stmt = $cassandra->prepare('INSERT INTO users (id, first_name, last_name)
    VALUES (:id, :first_name, :last_name)');

    // Executes a prepared statement.
    $values = array('first_name' => 'Lau', 'last_name' => 'Kok', 'id' => '4');
    $result = $cassandra->execute($stmt, $values);
    var_dump($result);

    // Delete:
    // Prepares a statement.
    $stmt = $cassandra->prepare('DELETE FROM users WHERE id = :id');

    // Executes a prepared statement.
    $values = array('id' => '4');
    $result = $cassandra->execute($stmt, $values);
    var_dump($result);

    // Closes the connection.
    $cassandra->close();
}
