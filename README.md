# Cassandra

## Start Cassandra

1. Download Cassandra and store the file in C drive or the location you like.

2. Install Cassandra to your system variable.

    1. Right-click the My Computer icon on your desktop and select Properties
    2. Click the Advanced tab
    3. Click the Environment Variables button
    4. Under System Variables, look for `path`
    6. Enter the variable value as the installation path for the Cassandra

        eg. `C:\Apache\cassandra-2.1.8\bin`

    7. Click OK
    8. Click Apply Changes

3. Set the JAVA_HOME Variable.

    1. Right-click the My Computer icon on your desktop and select Properties
    2. Click the Advanced tab
    3. Click the Environment Variables button
    4. Under System Variables, click New
    5. Enter the variable name as JAVA_HOME
    6. Enter the variable value as the installation path for the Java Development Kit

        If your Java installation directory has a space in its path name, you should use the shortened path name (e.g. C:\Java\jre) in the environment variable, instead.

    7. Click OK
    8. Click Apply Changes

3. Open the Windows CMD, type

    `cassandra`

## Install CQLSH

1. Install Python 2.7.

    Make sure `;C:\Python27` is appended to the value of path in the System Variables.

    Note that Thrift does indeed explicitly not support Python 3, it's metadata is marked as supporting Python 2 only, and installing it gives you a Syntax error.

    The following syntax:

    `except ImportError, e:`

    was deprecated in Python 2.7 and removed in Python 3.x. Nowadays, you use the as keyword:

    `except ImportError as e:`

    ref:

    * http://stackoverflow.com/questions/17390367/does-cql-support-python-3
    * http://stackoverflow.com/questions/19142231/cassandra-file-cqlsh-line-95-except-importerror-e

2. Download the Python Thrift module from the below link to c:/

    https://drive.google.com/file/d/0Bw3sqswQlMb4UHpNSHJtMVdEY1U/edit?usp=sharing

    or

    https://pypi.python.org/pypi/thrift

3. Open CMD and type,

    `cd C:\thrift-0.9.1`
    `python setup.py install`

## Open CQLSH

1. Open the Windows CMD, type

    `cd C:\Apache\cassandra-2.1.8\bin`

    `python cqlsh`

    ref:

    * https://www.youtube.com/watch?v=KCitXBThphY
    * http://www.edureka.co/blog/how-to-open-cqlsh-of-cassandra-installed-on-windows/
    * https://confluence.atlassian.com/display/DOC/Setting+the+JAVA_HOME+Variable+in+Windows
    * http://www.howtogeek.com/197947/how-to-install-python-on-windows/

# DataStax (Not Recommended)

1. Download from DataStax website and install it.

    ref:

    * http://www.datastax.com/2012/01/getting-started-with-apache-cassandra-on-windows-the-easy-way
    * http://www.planetcassandra.org/create-a-keyspace-and-table/
    * http://docs.datastax.com/en/getting_started/doc/getting_started/gettingStartedCassandraIntro.html
    * http://docs.datastax.com/en/getting_started/doc/getting_started/gsKeyConDataMdl.html?scroll=gsKeyConDataMdl__key-concepts
    * https://wiki.apache.org/cassandra/GettingStarted
    * http://docs.datastax.com/en/cql/3.1/cql/cql_intro_c.html

# CQL

1. Keyspace

    A keyspace is a container for your application data. It is similar to the schema in a relational database. The keyspace can include operational elements, such as replication factor and data center awareness.

    Create a keyspace,

    `CREATE KEYSPACE demo WITH REPLICATION = { 'class' : 'SimpleStrategy', 'replication_factor' : 1 };`

    List keyspaces,

    `DESC KEYSPACES;`

    Alter keyspace,

    alter KEYSPACE system_auth WITH replication =  { 'class' : 'SimpleStrategy', 'replication_factor' : 2};

    Drop keyspace,

    `DROP KEYSPACE demo;`

2. Select keyspace,

    `USE demo;`

3. Create table,

    ```
    CREATE TABLE users (
      id int,
      first_name varchar,
      last_name varchar,
      PRIMARY KEY (id)
    );
    ```

    List tables,

    `DESCRIBE TABLES;`

    To see the various attributes of the users table we have just created, type:

    `DESC SCHEMA;`

    Whether you use the keyword TABLE or COLUMNFAMILY, both are same(synonyms). I guess the keyword TABLE is introduced in the time of evolution of CQL3. So you can use any one of your statements. In short, a column family and a table are the same thing.

    * The name "column family" was used in the older Thrift API.
    * The name "table" is used in the newer CQL API.

    `CREATE COLUMNFAMILY users ( id int, first_name varchar, last_name varchar, PRIMARY KEY (id) );`

    List column families,

    `SELECT COLUMNFAMILY_NAME FROM SYSTEM.SCHEMA_COLUMNFAMILIES WHERE KEYSPACE_NAME = 'demo';`

    Or,

    `DESCRIBE COLUMNFAMILIES;`

    ref:

        * http://stackoverflow.com/questions/18824390/whats-the-difference-between-creating-a-table-and-creating-a-columnfamily-in-cas
        * http://stackoverflow.com/questions/16096009/creating-column-family-or-table-in-cassandra-while-working-datastax-apiwhich-us

4. Insert data,

    ```
    INSERT INTO users (id, first_name, last_name)
    VALUES (1, 'Fred', 'Smith');
    INSERT INTO users (id, first_name, last_name)
    VALUES (2, 'Mary', 'Taylor');
    INSERT INTO users (id, first_name, last_name)
    VALUES (3, 'Bob', 'Jackson');
    ```

5. Update data,

    ```
    UPDATE users
      SET
        first_name = 'Fred1',
        last_name = 'Smith1'
      WHERE id = 1;
    ```

6. Delete a data,

    `DELETE FROM users WHERE id = 1;`

7. Select all data,

    `SELECT * FROM users;`

8. Drop table,

    `DROP TABLE tablename;`

    ref:

    * http://www.planetcassandra.org/create-a-keyspace-and-table/
    * http://docs.datastax.com/en/cql/3.1/cql/cql_intro_c.html

## Security

1. Go to C:\Cassandra\conf/cassandra.yaml,

    Comment out,

    `#authenticator: AllowAllAuthenticator`

    Add a new line,

    `authenticator: PasswordAuthenticator`

2. Restart Cassandra and log in with the Cassandra superuser,

    `cd c:\cassandra\bin`

    `python cqlsh localhost -u cassandra -p cassandra`

3. Once you get in, your first task should be to create another super user account,

    `CREATE USER root WITH PASSWORD 'bacon' SUPERUSER;`

    `CREATE USER admin WITH PASSWORD 'password';`

    List users,

    `LIST USERS;`

    Drop a user,

    `DROP USER user_name`

4. Open another CMD,

    `cd c:\cassandra\bin`

    `python cqlsh localhost -u admin -p password`

    Do whatever as usual.

    ref:

    * http://stackoverflow.com/questions/22213786/how-do-you-create-the-first-user-in-cassandra-db
    * http://stackoverflow.com/questions/10241666/setting-username-and-password-for-cassandra-database
    * http://docs.datastax.com/en/cql/3.0/cql/cql_reference/grant_r.html

## Grant

1. You will also want to set the auhorizer to CassandraAuthorizer in cassandra.yaml,

    `authorizer:CassandraAuthorizer`

2. Configure the replication factor for the system_auth keyspace.

    `alter KEYSPACE system_auth WITH replication =  { 'class' : 'SimpleStrategy', 'replication_factor' : 2};`

3. Restart Cassandra and log in with the Cassandra superuser,

    `cd c:\cassandra\bin`

    `python cqlsh localhost -u root -p bacon`

4. Grant users,

    `GRANT ALL ON KEYSPACE demo TO admin;`

    ref:

    * http://stackoverflow.com/questions/22215621/error-when-trying-to-alter-keyspace-replication-factor-with-cassandra-for-system
    * http://stackoverflow.com/questions/15343459/cassandra-access-control
    * http://docs.datastax.com/en/cql/3.0/cql/cql_reference/grant_r.html

# Cassandra + PHP

1. Currently it is very diffuclt to use PHP with Cassandra as Cassandra has no PHP driver officially, but has some contributed solutions.

2. Recommended library:

    * Cassandra Universal Driver - https://github.com/uri2x/php-cassandra

    ref:

    * http://blog.carlesmateo.com/2014/07/31/begin-developing-with-cassandra/
